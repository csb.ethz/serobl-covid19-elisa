# This file is part of the following manuscript:
# Title:   Initial characterisation of commercially available ELISA tests and
#          the immune response of the clinically correlated SARS-CoV-2 biobank
#          "SERO-BL-COVID-19" collected during the pandemic onset in Switzerland
#
# (C) Copyright 2020 ETH Zurich, D-BSSE, Hans-Michael Kaltenbach, Janina Linnik, Julia Deichmann
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php

library(dplyr)
library(plyr)
library(tidyr)
library(here)
library(ggplot2)
library(cowplot)
library(viridis)


source(file.path(here::here(), "utils", "gather_elisa_data.R"))
source(file.path(here::here(), "paper-figures", "theme_paper.R"))

# Load data
elisa_euro <- readRDS(file.path(here::here(), "data", "SEROBL_ELISA_Euroimmun.RDS"))
elisa_edi <- readRDS(file.path(here::here(), "data", "SEROBL_ELISA_EDI.RDS"))
data_base <- readRDS(file.path(here::here(), "data", "SEROBL_BASE_DATA.RDS")) %>%
  dplyr::mutate(
    when_ill = factor(
      when_ill,
      levels = rev(c("no_restrictions", "help_needed", "bedridden", "--")),
      labels = rev(c("No restrictions", "Help needed", "Bedridden", "Unknown"))
    )
  )
data_comb <- gather_elisa_data(data_base, elisa_euro, elisa_edi) %>%
  dplyr::filter(pcr == "positive") %>%
  dplyr::mutate(od_diff = od_sample - od_cutoff)

# Plot geom_smooth vs days post symptoms
plot_od_smooth <- function(data, y = "od_ratio") {

  data <- data[data$days_post_symptoms < 6.5 * 7, ]
  p <- ggplot(data)
  if (y == "od_ratio"){
    p <- p + 
      geom_rect(
        lwd = 0.3,
        fill = "grey80",
        aes(
          xmin = -Inf, xmax = Inf,
          ymin = log2(od_cutoff_uncertain / od_cutoff),
          ymax = log2(0.99)
        )
      ) +
      geom_point(
        aes(days_post_symptoms / 7, log2(od_ratio), color = when_ill),
        pch = 21, alpha = 0.8, size = 0.7
      ) +
      geom_smooth(
        aes(
          days_post_symptoms / 7, log2(od_ratio),
          group = when_ill,
          color = when_ill,
          fill = when_ill
        ),
        lwd = 0.7, alpha = 0.4
      ) +
      scale_y_continuous(
        name = bquote(Log[2] ~ "fold change (OD sample / OD cutoff)"),
        breaks = seq(-4, 7)
      )
  }
  if (y == "od_diff"){
    p <- p + 
      geom_hline(yintercept = 0, lty = 2) +
      geom_point(
        aes(days_post_symptoms / 7, od_diff, color = when_ill),
        pch = 21, alpha = 0.8, size = 0.7
      ) +
      geom_smooth(
        aes(
          days_post_symptoms / 7, od_diff,
          group = when_ill,
          color = when_ill,
          fill = when_ill
        ),
        lwd = 0.7, alpha = 0.4
      ) +
      scale_y_continuous(
        name = "OD sample - OD cutoff",
        breaks = seq(-4, 7)
      )
  }
  p <- p + 
    facet_wrap(elisa_test_name ~ elisa_type, ncol = 4) +
    scale_x_continuous(
      breaks = seq(0, 7),
      name = "Weeks after symptoms onset"
    ) +
    
    background_grid() +
    scale_color_viridis(
      name = "Situation when ill", begin = 0, end = 0.8, discrete = T
    ) +
    scale_fill_viridis(
      name = "Situation when ill", begin = 0.2, end = 0.9, discrete = T
    ) +
    theme_paper(
      legend_pos = "top", legend_dir = "horizontal",
      legend_margin = -0.2
    ) +
    theme(
      strip.text = element_text(vjust = 1.2),
      axis.line = element_line()
    )
  p
}
p_smooth <- plot_od_smooth(data_comb)

# Same plot in linear scale
p_smooth_lin <- plot_od_smooth(data_comb, y = "od_diff")

# Save pdf and RDS files
ggsave(
  filename = file.path(here::here(), "paper-figures", "2-splines_logscale.pdf"),
  p_smooth,
  width = 17, height = 8, units = "cm", dpi = 300
)
ggsave(
  filename = file.path(here::here(), "paper-figures", "2-splines_linscale.pdf"),
  p_smooth_lin,
  width = 17, height = 8, units = "cm", dpi = 300
)
saveRDS(p_smooth, file=file.path(here::here(), "paper-figures", "2-splines_logscale.RDS"))
saveRDS(p_smooth_lin, file=file.path(here::here(), "paper-figures", "2-splines_linscale.RDS"))

