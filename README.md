[![Project Status](http://www.repostatus.org/badges/latest/active.svg)](http://www.repostatus.org/#active)
[![pipeline status](https://gitlab.com/csb.ethz/serobl-covid19-elisa/badges/master/pipeline.svg)](https://gitlab.com/csb.ethz/serobl-covid19-elisa/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-grey.svg)](https://opensource.org/licenses/MIT)

## Information

This repository contains data and source files associated with the following manuscript: <br>
Initial characterisation of commercially available ELISA tests and the immune response of the clinically correlated SARS-CoV-2 biobank "SERO-BL-COVID-19" collected during the pandemic onset in Switzerland, __2020__.

Corresponding authors:
* Fabian Rudolf <fabian.rudolf@bsse.ethz.ch>
* Miodrag Savic <miodrag.savic@usb.ch>

## Content

* `data`: ELISA results and baseline characteristics of participants.
* `paper-tables`: compile the Rmd files in this folder to reproduce the tables in the paper.
* `paper-figures`: run the R scripts in this folder to reproduce the figures in the paper.

## Requirements

* R version >= 3.6
* R package `checkpoint` (0.4.9)
* Execute `setup.R` to install all required R dependencies into a checkpoint folder. Your R session will then only use those packages.

## Authors

* Hans-Michael Kaltenbach <michael.kaltenbach@bsse.ethz.ch>
* Jana Linnik <janina.linnik@bsse.ethz.ch>
* Julia Deichmann <julia.deichmann@bsse.ethz.ch>

## Contact

If you encounter a problem, use the issue tracker or contact:
* Jana Linnik <janina.linnik@bsse.ethz.ch>

## Licence

Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php

## References

* Preprint available: Kaltenbach et al., Initial characterisation of commercially available ELISA tests and the immune response of the clinically correlated SARS-CoV-2 biobank "SERO-BL-COVID-19" collected during the pandemic onset in Switzerland, __2020__. (https://www.medrxiv.org/content/10.1101/2020.07.05.20145888v2)
