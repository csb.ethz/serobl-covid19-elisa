# This file is part of the following manuscript:
# Title:   Initial characterisation of commercially available ELISA tests and
#          the immune response of the clinically correlated SARS-CoV-2 biobank
#          "SERO-BL-COVID-19" collected during the pandemic onset in Switzerland
#
# (C) Copyright 2020 ETH Zurich, Hans-Michael Kaltenbach, Janina Linnik, Julia Deichmann
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php

#' @title Categorise data by days since symptom onset
#' @param data A data frame with qualitative results and days since symptom onset.
#' @param categories Vector with number of days separating categories
#' @param labels Vector with category labels
#' @return A list of data frames.
#' @importFrom dplyr filter
data_by_symptom_onset <- function(data, categories, labels) {
  tmp <- list()
  no_cat <- length(categories)

  tmp[[1]] <- dplyr::filter(data, (days_post_symptoms < categories[1]) | pcr == "negative")
  tmp[[1]]["days_category"] <- labels[1]

  if (no_cat > 1) {
    for (i in 1:(no_cat - 1)) {
      tmp[[i + 1]] <- dplyr::filter(data, (days_post_symptoms >= categories[i] & days_post_symptoms < categories[i + 1]) | pcr == "negative")
      tmp[[i + 1]]["days_category"] <- labels[i + 1]
    }
  }

  tmp[[no_cat + 1]] <- dplyr::filter(data, (days_post_symptoms >= categories[no_cat]) | pcr == "negative")
  tmp[[no_cat + 1]]["days_category"] <- labels[no_cat + 1]

  return(tmp)
}
